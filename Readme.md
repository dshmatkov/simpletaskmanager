Api for simple task manager with resources and auth. Send e-mail every day with tasks which must be finished in this day.

Install
-------
1. Install PostgreSQL and create database 'test' or set custom database in settings
2. Install virtualenv
3. `mkvirtualenv test-app`
4. `workon test-app`
5. `pip install -Ur requirements.txt`
6. `./manage.py migrate`

Run tests
---------
`./manage.py test`

Run server
----------
`./manage.py runserver`

Main components
===============
- Auth (apps/authapp)
- Core functionality (apps/core)

API
===

Auth api
--------
Supported methods: `POST`

Endpoints: `../sign_in/`, `../sing_up/`, `../sign_out/`

Core api
--------

Endpoints: 

`../{resource_name}/`       (`GET`, `POST`)

`../{resource_name}/{id}`   (`GET`, `PATCH`, `DELETE`)

Resources: `user`, `profile`, `project`, `task`

`{id}` must be Integer

Each resource must be valid json object with its members.

