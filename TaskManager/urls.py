from __future__ import unicode_literals, absolute_import

from django.conf.urls import include, url
from django.contrib import admin

from apps.authapp.urls import urlpatterns as auth_urls
from apps.core.urls import urlpatterns as core_urls

admin.autodiscover()

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
]

urlpatterns.extend(auth_urls)
urlpatterns.extend(core_urls)
