from __future__ import unicode_literals, absolute_import

import datetime
from django.test import TestCase

from apps.core.models import User, Project, Task


class ModelsTest(TestCase):

    def test_profile_creation(self):
        user = User(username='u', email='aa@aa.aa')
        user.set_password('123')
        user.save()

        try:
            profile = user.profile
        except Exception:
            assert False, 'Profile has not been created.'

    def test_binding_task_to_project(self):
        project = Project(title='title', description='test')
        project.save()

        task = Task(
            title='title',
            description='description',
            due_date=datetime.date.today() + datetime.timedelta(days=1),
            project=project,
        )
        task.save()

        self.assertItemsEqual(
            project.tasks.all(),
            [task],
        )

    def test_many_to_many(self):
        project_1 = Project(title='title1', description='test1')
        project_1.save()

        project_2 = Project(title='title2', description='test2')
        project_2.save()

        user_1 = User(username='adf', email='aa@aa.aa')
        user_1.save()

        user_2 = User(username='fdsa', email='bb@bb.bb')
        user_2.save()

        user_1.projects.add(project_1, project_2)

        project_1.assignees.add(user_2)
        project_2.assignees.add(user_2)

        self.assertItemsEqual(
            [user_1, user_2],
            project_1.assignees.all(),
        )
        self.assertItemsEqual(
            [user_1, user_2],
            project_2.assignees.all(),
        )

        self.assertItemsEqual(
            [project_1, project_2],
            user_1.projects.all()
        )
        self.assertItemsEqual(
            [project_1, project_2],
            user_2.projects.all()
        )
