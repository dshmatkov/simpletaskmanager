from __future__ import unicode_literals, absolute_import

import json

from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.test import TestCase

from apps.core.models import Project


class ResourcesTest(TestCase):

    def setUp(self):
        self.user = User(
            username='test',
            email='test@...',
        )
        self.user.set_password('test')
        self.user.save()
        self.client.post(
            '/sign_in/',
            {
                'username': 'test',
                'password': 'test',
            }
        )

    def test_read_list(self):
        response = self.client.get('/user/')
        self.assertItemsEqual(
            [{
                "id": self.user.id,
                "email": "test@...",
                "tasks": [],
                "projects": [],
                "profile": self.user.profile.id,
                "username": "test",
            }],
            json.loads(response.content)
        )

    def test_create_project(self):
        self.user.profile.role = 'MAN'
        self.user.profile.save()

        response = self.client.post(
            '/project/',
            {
                'title': 'a',
                'description': 'b',
            }
        )

        result = json.loads(response.content)
        id_ = result.pop('id')
        self.assertItemsEqual(
            {
                'title': 'a',
                'description': 'b',
                'assignees': [],
            },
            result
        )

        response = self.client.get('/project/{}'.format(id_))
        self.assertItemsEqual(
            {
                'id': id_,
                'title': 'a',
                'description': 'b',
                'assignees': [],
            },
            json.loads(response.content)
        )

    def test_update_project(self):
        self.user.profile.role = 'MAN'
        self.user.profile.save()

        project = Project(
            title='a',
            description='b',
        )
        project.save()

        response = self.client.patch(
            '/project/{}'.format(project.id),
            json.dumps({
                'title': 'c',
                'description': 'd',
                'assignees': [self.user.id],
            }),
            content_type='application/json',
        )
        self.assertItemsEqual(
            {
                'id': project.id,
                'title': 'c',
                'description': 'd',
                'assignees': [self.user.id],
            },
            json.loads(response.content)
        )

        response = self.client.get(
            '/project/{}'.format(project.id),
        )
        self.assertItemsEqual(
            {
                'id': project.id,
                'title': 'c',
                'description': 'b',
                'assignees': [self.user.id],
            },
            json.loads(response.content)
        )

    def test_delete_project(self):
        self.user.profile.role = 'MAN'
        self.user.profile.save()

        project = Project(
            title='a',
            description='b',
        )
        project.save()

        id_ = project.id

        response = self.client.delete(
            '/project/{}/'.format(project.id)
        )
        self.assertEqual(200, response.status_code)

        with self.assertRaises(ObjectDoesNotExist):
            Project.objects.get(pk=id_)

    def test_M2M(self):
        self.user.profile.role = 'MAN'
        self.user.profile.save()

        project_1 = Project(
            title='a',
            description='b',
        )
        project_1.save()

        project_2 = Project(
            title='c',
            description='d',
        )
        project_2.save()

        user_2 = User(
            email='aaa@...',
            username='a',
        )
        user_2.set_password('asdfasdfasdf')
        user_2.save()

        self.client.patch(
            '/project/{}'.format(project_1.id),
            json.dumps({
                'assignees': [self.user.id, user_2.id]
            }),
            content_type='application/json',
        )
        self.client.patch(
            '/project/{}'.format(project_2.id),
            json.dumps({
                'assignees': [self.user.id, user_2.id]
            }),
            content_type='application/json',
        )

        response = self.client.get('/project/')
        got_result = [item['assignees'] for item in json.loads(response.content)]

        for item in got_result:
            self.assertItemsEqual(
                [self.user.id, user_2.id],
                item
            )
