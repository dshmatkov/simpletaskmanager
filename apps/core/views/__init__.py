from __future__ import unicode_literals, absolute_import

from .resource import ResourceView
from .resources import ResourcesView
