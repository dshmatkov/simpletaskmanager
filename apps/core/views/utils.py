from __future__ import unicode_literals, absolute_import

from ..models import Profile


def check_permissions(user, method, resource, id_=None):
    """Check if action permitted to user with resource according to the
    business logic.
    :type user: django.contrib.auth.models.User
    :type method: str
    :type resource: str
    :type id_: int
    :rtype: bool
    """

    if method == 'GET':
        return True

    if user.profile.role == Profile.MANAGER:
        return True

    if resource in ('user', Profile.NAME) and user.id == id_:
        return True

    return False
