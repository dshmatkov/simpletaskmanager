from __future__ import unicode_literals, absolute_import

from django.core.exceptions import ObjectDoesNotExist
from rest_framework.views import APIView
from rest_framework.exceptions import NotFound, ValidationError
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from ..models import RESOURCES
from ..serializers import SERIALIZERS
from .utils import check_permissions


class ResourceView(APIView):
    """Handles requests which send on '../resource/{id}/' endpoint.
    Supported methods: 'GET', 'PATCH', 'DELETE'
    """
    permission_classes = (IsAuthenticated, )

    def check_permissions(self, request):
        super(ResourceView, self).check_permissions(request)

        context_kwargs = request.parser_context.get('kwargs', {})
        resource = context_kwargs.get('resource')
        id_ = context_kwargs.get('id')
        operation = request.method

        if resource not in RESOURCES:
            raise NotFound("Resource not found")

        is_permitted = check_permissions(request.user, operation, resource, id_)
        if not is_permitted:
            self.permission_denied(request, 'Not authorized for this action')

    def get(self, request, resource, id_):
        model = RESOURCES.get(resource)

        try:
            result_object = model.objects.get(pk=int(id_))
            serializer = SERIALIZERS.get(model)(result_object)
            return Response(data=serializer.data)

        except ObjectDoesNotExist:
            raise NotFound('Object not found')

    def patch(self, request, resource, id_):
        model = RESOURCES.get(resource)

        try:
            updating_object = model.objects.get(pk=int(id_))
            serializer = SERIALIZERS.get(model)(
                updating_object, data=request.data, partial=True
            )

            if not serializer.is_valid():
                raise ValidationError(serializer.errors)

            serializer.save()
            return Response(data=serializer.data)

        except ObjectDoesNotExist:
            raise NotFound('Object not found')

    def delete(self, request, resource, id_):
        model = RESOURCES.get(resource)

        try:
            deleting_object = model.objects.get(pk=int(id_))
            deleting_object.delete()

            return Response(data='Ok')

        except ObjectDoesNotExist:
            raise NotFound('Object not found')
