from __future__ import unicode_literals, absolute_import

from rest_framework.views import APIView
from rest_framework.exceptions import NotFound, ValidationError
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from ..models import RESOURCES
from ..serializers import SERIALIZERS
from .utils import check_permissions


class ResourcesView(APIView):
    """Handles requests which send on '../resource/' endpoint.
    Supported methods: 'GET', 'POST'
    """
    permission_classes = (IsAuthenticated, )

    def check_permissions(self, request):
        super(ResourcesView, self).check_permissions(request)

        context_kwargs = request.parser_context.get('kwargs', {})
        resource = context_kwargs.get('resource')
        operation = request.method

        if resource not in RESOURCES:
            raise NotFound("Resource not found")

        is_permitted = check_permissions(request.user, operation, resource)
        if not is_permitted:
            self.permission_denied(request, 'Not authorized for this action')

    def get(self, request, resource):
        model = RESOURCES.get(resource)

        objects = model.objects.all()
        serializer = SERIALIZERS.get(model)(objects, many=True)
        return Response(data=serializer.data)

    def post(self, request, resource):
        model = RESOURCES.get(resource)

        serializer = SERIALIZERS.get(model)(data=request.data)

        if not serializer.is_valid():
            raise ValidationError(serializer.errors)

        serializer.save()
        return Response(data=serializer.data)
