from __future__ import unicode_literals, absolute_import

from django.contrib.auth.models import User
from django.db import models as ms
from .project import Project


class Task(ms.Model):
    NAME = 'task'

    title = ms.CharField(max_length=500)
    description = ms.CharField(max_length=1000)
    due_date = ms.DateField()

    project = ms.ForeignKey(
        Project,
        on_delete=ms.CASCADE,
        null=False,
        related_name='tasks',
    )
    assignee = ms.ForeignKey(
        User,
        on_delete=ms.SET_NULL,
        null=True,
        related_name='tasks',
    )
