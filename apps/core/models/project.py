from __future__ import unicode_literals

from django.db import models as ms
from django.contrib.auth.models import User


class Project(ms.Model):
    NAME = 'project'

    title = ms.CharField(max_length=100)
    description = ms.CharField(max_length=1000)

    assignees = ms.ManyToManyField(User, related_name='projects')
