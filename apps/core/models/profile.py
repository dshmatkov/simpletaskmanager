from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models as ms
from django.db.models.signals import post_save


class Profile(ms.Model):
    """Extends django auth User model with extra fields.
    """
    NAME = 'profile'

    user = ms.OneToOneField(User, related_name='profile', null=False)

    MANAGER = 'MAN'
    DEVELOPER = 'DEV'

    ROLES = (
        (MANAGER, 'Manager'),
        (DEVELOPER, 'Developer'),
    )

    role = ms.CharField(
        max_length=100,
        choices=ROLES,
        default=DEVELOPER,
    )


def create_profile(sender, **kwargs):
    """Provides inserting profile row on inserting user row.
    """
    user = kwargs['instance']
    if not kwargs['created']:
        return

    profile = Profile(user=user)
    profile.save()


post_save.connect(create_profile, sender=User)
