from __future__ import unicode_literals, absolute_import

from .profile import Profile
from .project import Project
from .task import Task
from django.contrib.auth.models import User


RESOURCES = {
    resource.NAME: resource
    for resource in [
        Profile,
        Project,
        Task,
    ]
}

RESOURCES['user'] = User
