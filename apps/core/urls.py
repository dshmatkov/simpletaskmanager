from __future__ import unicode_literals, absolute_import

from django.conf.urls import url
from .views import (
    ResourceView,
    ResourcesView,
)

urlpatterns = [
    url(r'^(?P<resource>[a-z_]+)/$', ResourcesView.as_view()),
    url(r'^(?P<resource>[a-z_]+)/(?P<id_>[0-9]+)', ResourceView.as_view()),
]
