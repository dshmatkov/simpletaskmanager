from __future__ import unicode_literals, absolute_import

import datetime

from celery.task import periodic_task
from django.core.mail import send_mass_mail
from .models import User, Task


@periodic_task(run_every=datetime.timedelta(days=1))
def sending_notifications():

    for user in User.objects.all():
        tasks = Task.objects.filter(
            assignee=user,
            due_date=datetime.date.today(),
        )

        tasks_text = '\n'.join([
            ' - {}'.format(task.title)
            for task in tasks
        ])
        text = (
            'This is daily task report. This tasks need to finish today:\n{}'
        ).format(
            tasks_text
        )

        send_mass_mail(
            'Daily Task Report',
            text,
            'from@example.com',
            [user.email],
            fail_silently=False,
        )
