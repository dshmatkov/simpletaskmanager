from __future__ import unicode_literals, absolute_import

from django.contrib.auth.models import User
from rest_framework import serializers as sr
from ..models import Project


class ProjectSerializer(sr.ModelSerializer):
    assignees = sr.PrimaryKeyRelatedField(
        many=True,
        queryset=User.objects.all(),
        required=False,
        read_only=False,
    )

    class Meta(object):
        model = Project
        fields = ('id', 'title', 'description', 'assignees')
