from __future__ import unicode_literals, absolute_import

from rest_framework import serializers as sr
from ..models import Profile


class ProfileSerializer(sr.ModelSerializer):
    user = sr.PrimaryKeyRelatedField(
        read_only=True
    )

    class Meta(object):
        model = Profile
        fields = ('id', 'role', 'user')
