from .profile import ProfileSerializer
from .project import ProjectSerializer
from .task import TaskSerializer
from .user import UserSerializer

SERIALIZERS = {
    serializer.Meta.model: serializer
    for serializer in [
        ProfileSerializer,
        ProjectSerializer,
        TaskSerializer,
        UserSerializer,
    ]
}
