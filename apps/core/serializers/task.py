from __future__ import unicode_literals, absolute_import

from django.contrib.auth.models import User
from rest_framework import serializers as sr
from ..models import Task, Project


class TaskSerializer(sr.ModelSerializer):
    project = sr.PrimaryKeyRelatedField(
        queryset=Project.objects.all(),
        required=False,
        read_only=False,
    )
    assignee = sr.PrimaryKeyRelatedField(
        queryset=User.objects.all(),
        required=False,
        read_only=False,
    )

    class Meta(object):
        model = Task
        fields = (
            'id', 'title', 'description', 'due_date', 'project', 'assignee'
        )
