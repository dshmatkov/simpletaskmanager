from __future__ import unicode_literals, absolute_import

from django.contrib.auth.models import User
from rest_framework import serializers as sr
from ..models import Task, Project


class UserSerializer(sr.ModelSerializer):
    tasks = sr.PrimaryKeyRelatedField(
        many=True,
        queryset=Task.objects.all(),
        required=False,
        read_only=False,
    )
    projects = sr.PrimaryKeyRelatedField(
        many=True,
        queryset=Task.objects.all(),
        required=False,
        read_only=False,
    )
    profile = sr.PrimaryKeyRelatedField(
        read_only=True
    )

    class Meta(object):
        model = User
        fields = (
            'id',
            'email',
            'tasks',
            'projects',
            'profile',
            'username',
            'password'
        )
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user = super(UserSerializer, self).create(validated_data)

        user.set_password(validated_data['password'])
        return user

    def update(self, instance, validated_data):
        user = super(UserSerializer, self).update(instance, validated_data)

        new_password = validated_data.get('password', None)
        if new_password is None:
            return user

        user.set_password(new_password)
        user.save()
        return user
