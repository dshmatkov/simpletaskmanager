from __future__ import unicode_literals, absolute_import

from django.contrib.auth import authenticate, login, logout
from django.middleware.csrf import get_token
from rest_framework.decorators import api_view
from rest_framework.exceptions import NotAuthenticated, ValidationError
from rest_framework.response import Response

from .serializers import RegisteredUserSerializer


@api_view(['POST'])
def sign_in(request):
    """Get credentials, check it and authenticate user.
    Body must be valid json object with 2 members: 'username', 'password'.
    :return: Ok response in case of success or raise exception
    """
    required_fields = {
        'username', 'password'
    }

    if set(request.data) != required_fields:
        return NotAuthenticated('Wrong auth data')

    user = authenticate(
        username=request.data['username'],
        password=request.data['password'],
    )

    if user is not None:
        login(request, user)
        token = get_token(request)
        return Response(data='Ok', headers={
            'X-CSRFToken': token,
        })

    else:
        raise NotAuthenticated('Bad credentials')


@api_view(['POST'])
def sign_out(request):
    logout(request)
    return Response(data='Ok')


@api_view(['POST'])
def sign_up(request):
    """Get user data and register it.
    Body must be valid json object with 3 members: 'username', 'password',
    'email'.
    :return: Response with valid json object with 2 members: 'username', 'email'
        or raise exception in case of fail
    """
    serializer = RegisteredUserSerializer(data=request.data)

    if not serializer.is_valid():
        errors = serializer.errors
        errors.pop('password', None)
        raise ValidationError(errors)

    serializer.save()
    return Response(serializer.data)
