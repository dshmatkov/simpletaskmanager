from __future__ import unicode_literals, absolute_import

import json

from django.contrib.auth.models import User
from django.test import TestCase


class RegistrationTest(TestCase):

    def test_user_registration(self):
        response = self.client.post(
            '/sign_in/',
            {
                'username': 'test',
                'password': 'qwer',
            }
        )
        self.assertEqual(403, response.status_code)

        response = self.client.post(
            '/sign_up/',
            {
                'username': 'test',
                'email': 'test@test.ru',
                'password': 'qwer',
            }
        )
        self.assertEqual(200, response.status_code)
        self.assertItemsEqual(
            json.loads(response.content),
            {
                'username': 'test',
                'email': 'test@test.ru',
            },
        )

        response = self.client.post(
            '/sign_in/',
            {
                'username': 'test',
                'password': 'qwer1',
            }
        )
        self.assertEqual(403, response.status_code)

        response = self.client.post(
            '/sign_in/',
            {
                'username': 'test',
                'password': 'qwer',
            }
        )
        self.assertEqual(200, response.status_code)

    def test_register_twice(self):
        response = self.client.post(
            '/sign_up/',
            {
                'username': 'test',
                'email': 'test@test.ru',
                'password': 'qwer',
            }
        )
        self.assertEqual(200, response.status_code)

        response = self.client.post(
            '/sign_up/',
            {
                'username': 'test',
                'email': 'test@test.ru',
                'password': 'qwer',
            }
        )
        self.assertEqual(400, response.status_code)

    def test_wrong_registration(self):
        response = self.client.post(
            '/sign_up/',
            {
                'username': 'test',
                'email': 'test@test.ru',
            }
        )
        self.assertEqual(400, response.status_code)


class AuthenticationTest(TestCase):

    def setUp(self):
        user = User(
            username='Test',
            email='aa@aa.aa',
        )
        user.set_password('test')
        user.save()

    def test_unauthorized_access(self):
        response = self.client.get('/user/')
        self.assertEqual(403, response.status_code)

    def test_sign_in_sign_out(self):

        # check unauthorized access
        response = self.client.get('/user/')
        self.assertEqual(403, response.status_code)

        # sign in
        response = self.client.post(
            '/sign_in/',
            {
                'username': 'Test',
                'password': 'test',
            },
        )
        self.assertEqual(200, response.status_code)

        # check access after authorization
        response = self.client.get('/user/')
        self.assertEqual(200, response.status_code)

        # sign out
        response = self.client.post('/sign_out/', {})
        self.assertEqual(200, response.status_code)

        # check access after sign out
        response = self.client.get('/user/')
        self.assertEqual(403, response.status_code)

    def test_sign_in_wrong_passw(self):
        response = self.client.post(
            '/sign_in/',
            {
                'username': 'Test',
                'password': 'test1',
            },
        )
        self.assertEqual(403, response.status_code)

    def test_sign_in_wrong_username(self):
        response = self.client.post(
            '/sign_in/',
            {
                'username': 'Test',
                'password': 'test1',
            },
        )
        self.assertEqual(403, response.status_code)
