from __future__ import unicode_literals

from django.contrib.auth.models import User
from rest_framework import serializers as sr


class RegisteredUserSerializer(sr.ModelSerializer):

    class Meta(object):
        model = User
        fields = ('email', 'username', 'password')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user = User(
            username=validated_data['username'],
            email=validated_data['email'],
        )
        user.set_password(validated_data['password'])
        user.save()
        return user
