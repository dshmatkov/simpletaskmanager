from __future__ import unicode_literals, absolute_import

from django.conf.urls import url
from .views import sign_in, sign_out, sign_up


urlpatterns = [
    url('^sign_in/$', sign_in),
    url('^sign_out/$', sign_out),
    url('^sign_up/$', sign_up),
]
